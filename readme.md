# E-pasta sūtīšana izmantojot serverless AWS Lambda servisu
## Amazon Simple Email Service (SES) konfigurācija
- [ ] Atver AWS konsoli https://952122846739.signin.aws.amazon.com/console un ieej savā kontā, pārliecinies vai augšējā labā stūrī reģions izvēlēts *Ireland* (Europe (Ireland) eu-west-1). Ja nē, nomaini to uz *Ireland*
- [ ] Ieej Amazon SES konsolē https://eu-west-1.console.aws.amazon.com/sesv2/home?region=eu-west-1#/verified-identities un uzstādi Amazon SES Identity
    1. Labajā pusē spied uz *"Create identity"*
    1. Identity type - Email address
    1. Email address - ieraksti savu e-pasta adresi (labāk izmantot tādu, kam nevarētu būt savienojuma problēmas, piemēram, @gmail.com)
    1. Spied *"Create identity"*
- [ ] Ieej e-pastā, ko pievienoji kā identity un apstiprini to, atjauno lapu, tam vajadzētu mainīt "Identity status" uz "Verified"
- [ ] Pārbaudi, vai SES strādā nosūtot sev testa e-pastu
    1. Atverot savu identity, labajā pusē spied *"Send test email"*
    1. Email format - Formatted
    1. From-address - atstāj nemainītu
    1. Scenario - Custom
    1. Custom recipient - ierakstu savu verificēto e-pasta adresi
    1. Subject - "Testa epasts no Amazon SES"
    1. Body - ieraksti sev kādu ziņojumu
    1. Pārējos laukus atstāj tukšus un spied *"Send test email"*
    1. Pārbaudi savu e-pastu, vai esi saņēmis testa e-pastu (ja e-pasts nav Iesūtnē, pārbaudi mēstules un atļauj adresātu)

## Lambda funkcijas izveide
- [ ] Atver AWS Lambda servisu - https://eu-west-1.console.aws.amazon.com/lambda/home?region=eu-west-1#/functions un spied *"Create function"*  labajā pusē
    1. Atstāj "Author from scratch"
    1. Function name - email-test-tavsVards
    1. Runtime - Python 3.8
    1. Architecture - x86_64
    1. Spied uz "Change default execution role" -> Use and existing role -> Izvēlies "LambdaRole" un spied *"Create function"*
- [ ] *Code* tab aizvieto lambda_function.py ar zemāk esošo kodu. Zemāk esošajā kodā aizvieto \<RECIPIENT\> un \<SENDER\> ar savu e-pasta adresi, ko pirms tam verificēji SES.
    ```python
    import boto3
    from botocore.exceptions import ClientError

    TO = "<RECIPIENT>"
    FROM = "<SENDER>"

    BODY_TEXT = ("Email body for recipients with non-HTML email clients\r\n"
                "Amazon SES Test (Python)\r\n"
                "This email was sent with Amazon SES using the "
                "AWS SDK for Python (Boto)."
                )

    BODY_HTML = """<html>
    <head></head>
    <body>
        <h1>Below email for HTML clients</h1>
        <p>This e-mail was sent from Lambda using Simple Email Service and Boto3</p>
    </body>
    </html>
                """

    ses = boto3.client('ses', region_name="eu-west-1")

    def lambda_handler(event, context):
        # TODO implement
        try:
            response = ses.send_email(
                Destination={
                    'ToAddresses': [TO]
                },
                Message={
                    'Body': {
                        'Html': {
                            'Charset': "UTF-8",
                            'Data': BODY_HTML
                        },
                        'Text': {
                            'Charset': "UTF-8",
                            'Data': BODY_TEXT
                        }
                    },
                    'Subject': {
                        'Charset': "UTF-8",
                        'Data': "Email sent from Lambda using SES"
                    }
                },
                Source=FROM
            )
        except ClientError as e:
            print(e.response['Error']['Message'])
        else:
            print("Email sent! Message ID:"),
            print(response['MessageId'])
    ```
    Saglabā izmaiņas, spiežot Deploy (augšā virs koda)
- [ ] Izveido Test event, spiežot "Test"
    1. Izvēlies "Create new test event"
    1. Event template atstāj hello-world
    1. Event name - EmailTestEvent
    1. Spied "Create"
- [ ] Notestē kodu, izmantojot izveidoo test eventu
    1. Ej uz *Test* tab
    1. Izvēlies Saved event -> EmailTestEvent
    1. Spied *Test* labajā pusē
- [ ] Pārbaudi e-pastu (iesūtni vai mēstules) un 
    *  🎉 pievieno uzdevumam komentārā ekrānšāviņu ar saņemto e-pastu

# Aplikācijas uzstādīšana izmantojot CloudShell un PaaS - AWS Elastic Beanstalk
## Aplikācijas uzstādīšana caur CloudShell
- [ ] Atver https://eu-west-1.console.aws.amazon.com/cloudshell/home?region=eu-west-1# un izpildi sekojošu komandu, lai noklonētu testa aplikāciju, tad ieej noklonētajā repozitorijā
    ```sh
    git clone https://github.com/aws-samples/eb-java-scorekeep.git
    cd eb-java-scorekeep
    ```
- [ ] Izpildi kodu, lai izveidotu S3 bucket, kur tiks uzglabāti Elastic Beanstalk artifakti. Pirms tam aizvieto tavsVards ar savu vārdu
    ```sh
    VARDS="tavsVards"
    BUCKET_NAME=beanstalk-artifacts-$VARDS
    echo $BUCKET_NAME > bucket-name.txt
    aws s3 mb s3://$BUCKET_NAME
    ```
- [ ] Izveido CF stack, kas izveidos visus nepieciešamos resursus
    ```sh
    sed -i "s/Scorekeep/Scorekeep-$VARDS/" template.yml
    git archive --format=zip HEAD > package.zip
    aws cloudformation package \
        --template-file template.yml \
        --s3-bucket $BUCKET_NAME \
        --output-template-file out.yml
    aws cloudformation deploy \
        --template-file out.yml \
        --stack-name scorekeep-$VARDS \
        --capabilities CAPABILITY_NAMED_IAM
    ```
- [ ] Seko līdzi progresam https://eu-west-1.console.aws.amazon.com/cloudformation/home?region=eu-west-1#/stacks?filteringStatus=active&filteringText=&viewNested=true&hideStacks=false atverot savu stacku un tab *Events*. Kad stack ir gatavs, atver https://eu-west-1.console.aws.amazon.com/elasticbeanstalk/home?region=eu-west-1#/applications un atrodi savu aplikāciju, meklējot pēc vārda, tad atver to un atver piesaistītā environment URL.
    * 🎉 Pievieno uzdevumam komentāru ar ekrānšāviņu ar ejošo aplikāciju
- [ ] Izlasi instrukcijas un pamēģini uzspēlēt Tiic Tac Toe

## Resursu tīrīšana
- [ ] CloudShell izpildi sekojošo komandu, lai izdzēstu stack.
    ```sh
    aws cloudformation delete-stack --stack-name scorekeep-$VARDS
    ```
    Lai apskatītu progresu, atjauno lapu izmantojot pārlūka vai AWS atjaunošanas pogu labējā augšējā stūrī. https://eu-west-1.console.aws.amazon.com/cloudformation/home?region=eu-west-1#/stacks?filteringStatus=deleted&filteringText=&viewNested=true&hideStacks=false&stackId= meklējot pēc sava vārda vari pārliecināties, ka statuss norāda *DELETE_COMPLETE*
